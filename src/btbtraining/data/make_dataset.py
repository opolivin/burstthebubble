# -*- coding: utf-8 -*-
"""This scripts loads data from a postgres table and saves it into
predefined categories in the data/raw folder. A postgres table is called
"NEWS_ANNOTATED" and is exported into that table by a scheduled task regularly.
"""

import argparse
import os
from pathlib import Path

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.btblabeling.db.datamodel.models import PostLabeled
from src.utils.utils_db import get_database_connection


parser = argparse.ArgumentParser()

load_dotenv()
BASE_PATH = os.getenv('BASE_PATH')
SUFFIX = '.txt'

if __name__ == "__main__":
    args = parser.parse_args()

    db_url = get_database_connection()
    engine = create_engine(db_url)

    Session = sessionmaker(bind=engine)
    session = Session()
    result = session.query(PostLabeled).filter(PostLabeled.exported.is_(False))
    for r in result:
        title = r.title.replace(':', '-')
        pubdate = r.pubdate.replace(':', '-')
        body = r.body
        source = r.source
        language = r.language
        label = r.label

        filename = pubdate + '-' + source + '-' + title + SUFFIX
        base_dir = Path(f'{BASE_PATH}/data/raw/{label}/{language}')
        base_dir.mkdir(parents=True, exist_ok=True)
        path = os.path.join(base_dir, filename)
        with open(path, 'w', encoding='utf-8') as f:
            f.write(f'{title}\n\n')
            f.write(f'{body}')
        session.query(PostLabeled).filter_by(id=r.id).update({"exported": True})
    session.commit()
