"""Script to split data/raw news data into train and validation sets
in the data/processed folder. However, there is no copying of files.
New csv files train.csv and valid.csv are created in the data/processed folder
that contain a path to the .txt file and its label
"""

import argparse
import os
import shutil
from pathlib import Path
from typing import List, Tuple

from dotenv import load_dotenv
from sklearn.model_selection import train_test_split


parser = argparse.ArgumentParser()
parser.add_argument("--clean", type=str, default=False)
parser.add_argument("--split", type=str)

load_dotenv()
BASE_PATH = os.getenv('BASE_PATH')
PATH = f'{BASE_PATH}/data/raw/'
OUT_PATH = f'{BASE_PATH}/data/processed/'


def save_train_val(split='train') -> None:
    """Utility function to save data into OUT_PATH
    folder.
    Args:
        split (str, optional): Folder/file.csv to be created.
                               Defaults to 'train'.
    """
    spath = Path(f'{OUT_PATH}/{split}')
    spath.mkdir(parents=True, exist_ok=True)
    filename = Path(f'{OUT_PATH}/{split}/{split}.csv')
    with open(filename, 'w', newline="", encoding='utf8') as outfile:
        for path in train_index:
            split_name = path.split('/')
            lbl = split_name[-3]
            outfile.write(f'{path},{lbl}\n')


def get_filepaths(path_to_raw_data: str) -> Tuple[List[str], List[str]]:
    """Created a list of all files in the data/raw folder.
    Args:
        path_to_raw_data (str): Path to the data/raw folder.

    Returns:
        _type_: _description_
    """
    paths = []
    strata = []
    for path, _, files in os.walk(path_to_raw_data):
        for name in files:
            split_name = path.split('/')
            label = split_name[-2]
            lang = split_name[-1]
            if label == 'raw':
                continue

            paths.append(os.path.join(path, name))
            strate_group = lang + '-' + label
            strata.append(strate_group)

    return paths, strata


if __name__ == '__main__':
    args = parser.parse_args()
    if args.clean:
        dirpath = Path(OUT_PATH)
        if dirpath.exists() and dirpath.is_dir():
            shutil.rmtree(dirpath)

    paths_lst, strata_lst = get_filepaths(PATH)
    train_index, val_index = train_test_split(
        paths_lst, stratify=strata_lst, random_state=42)

    if args.split == 'train':
        save_train_val(args.split)

    if args.split == 'valid':
        save_train_val(args.split)

    if args.split == 'train_valid':
        save_train_val('train')
        save_train_val('valid')
