""" Script that trains a Deep Learning classifier.
It classifies all news into categories defined in the train_config.yaml file
"""

import os
from typing import Dict, List
import warnings

import torch
from torch.utils.data import DataLoader, Dataset
from torch import nn
from torch.nn import CrossEntropyLoss
from transformers import AutoTokenizer, AutoModel, AutoConfig
import lightning.pytorch as pl
import yaml
from dotenv import load_dotenv

warnings.filterwarnings("ignore")


load_dotenv()
BASE_PATH = os.getenv('BASE_PATH')
MODEL_PATH = f'{BASE_PATH}/models/hf_models/xlm-roberta-base'
PROCESSED_DATA_PATH = f'{BASE_PATH}/data/processed'


class NewsData(Dataset):
    """Defines dataset for news classification
    """
    def __init__(self, path_to_datafile: str, model_path: str, label2idx_: Dict) -> None:
        """Class init

        Args:
            path_to_datafile (str): Path to a csv file that contains
                                    full path to .txt files with news
            model_path (str): Hugging Face model path
            label2idx (Dict): train_config.yaml file
        """
        self.path = path_to_datafile
        self.tokenizer = AutoTokenizer.from_pretrained(model_path)
        self.data = self.load_data()
        self.label2idx = label2idx_

    def load_data(self) -> List[str]:
        """
        Function that loads a list of paths to txt files with news
        """

        path = self.path
        with open(path, 'r', encoding='utf8') as infile:
            data = infile.readlines()
        return data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        filedata = self.data[idx].rsplit(',', maxsplit=1)
        filepath = filedata[0].strip()
        label = filedata[1].strip()
        with open(filepath, 'r', encoding='utf8') as newsfile:
            news = ''.join(newsfile.read().replace('\n\n', '. '))

        tokenized_news = self.tokenize(news)
        return tokenized_news, self.label2idx[label]

    def tokenize(self, text: str) -> Dict:
        """Hugging Face tokenizer

        Args:
            text (str): News text to tokenize

        Returns:
            Dict: Tokenized text (input_ids), attention_mask
        """

        tokenizer = self.tokenizer
        tokenized = tokenizer(
            text,
            return_attention_mask=True,
            padding='max_length',
            truncation=True,
            max_length=512,
            return_tensors='pt')
        return tokenized


class NewsClassifier(nn.Module):
    """News Classification PyTorch model"""

    def __init__(self, model_path: str, dropout_rate: float = 0.3):
        """Class Init

        Args:
            model_path (str): Path to Hugging Face model
            dropout_rate (float, optional): Dropout rate. Defaults to 0.3.
        """
        super(NewsClassifier, self).__init__()

        config = AutoConfig.from_pretrained(model_path, output_attentions=True)
        self.xlm = AutoModel.from_pretrained(model_path, config=config)
        self.d1 = nn.Dropout(dropout_rate)
        self.l1 = nn.Linear(768, 64)
        self.bn1 = nn.LayerNorm(64)
        self.d2 = nn.Dropout(dropout_rate)
        self.l2 = nn.Linear(64, NUM_CLASSES)

    def forward(self, input_ids, attention_mask):
        """Predictions of the model"""
        output = self.xlm(
            input_ids=input_ids,
            attention_mask=attention_mask,
            output_attentions=True
        )
        x = output.pooler_output
        x = self.d1(x)
        x = self.l1(x)
        x = self.bn1(x)
        x = nn.Tanh()(x)
        x = self.d2(x)
        x = self.l2(x)
        return x


class LitNewsClassifier(pl.LightningModule):
    """News Classifier wrapped into PyTorch Lightning class"""

    def __init__(self, model, lr: float, dropout_rate=0.3):
        super().__init__()
        self.model = model
        self.lr = lr

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.lr)
        lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1)
        return [optimizer], [lr_scheduler]

    def training_step(self, batch, batch_idx):
        """Training step defines the train loop.
        Necessary part of the Lightning module

        Args:
            batch (_type_): _description_
            batch_idx (_type_): _description_

        Returns:
            _type_: _description_
        """
        # it is independent of forward
        x, y = batch
        input_ids, attn_mask = x['input_ids'], x['attention_mask']
        input_ids = input_ids.view(input_ids.size(0), -1)
        out = self.model(
            input_ids=input_ids,
            attention_mask=attn_mask)
        loss = CrossEntropyLoss()(out, y)
        # Logging to TensorBoard (if installed) by default
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        input_ids, attn_mask = x['input_ids'], x['attention_mask']
        # print(input_ids.size())
        input_ids = input_ids.view(input_ids.size(0), -1)
        out = self.model(
            input_ids=input_ids,
            attention_mask=attn_mask)
        loss = CrossEntropyLoss()(out, y)
        self.log("val_loss", loss)


if __name__ == '__main__':

    with open('src/btbtraining/train/config_train.yaml', 'r', encoding='utf8') as f:
        config_train = yaml.safe_load(f)

    label2idx = config_train['labels']['label2idx']
    idx2label = {idx: lbl for lbl, idx in label2idx.items()}

    NUM_CLASSES = len(label2idx)
    DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
    print(DEVICE)

    newsdata_train = NewsData(PROCESSED_DATA_PATH + '/train/train.csv', MODEL_PATH, label2idx)
    newsdata_val = NewsData(PROCESSED_DATA_PATH + '/valid/valid.csv', MODEL_PATH, label2idx)
    nc = NewsClassifier(MODEL_PATH)
    model = LitNewsClassifier(nc, 0.1)
    # trainer.test(model)

    train_loader = DataLoader(newsdata_train)
    val_loader = DataLoader(newsdata_val)
    trainer = pl.Trainer(limit_train_batches=100, max_epochs=4, devices=1)
    trainer.fit(model=model, train_dataloaders=train_loader, val_dataloaders=val_loader)
