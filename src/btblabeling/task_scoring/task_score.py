"""Scirpt to assign a score to news for labeling.
It has two operation modes: assigning each news an equal constant score
or use a model to score news. News in which the neural network
is less confident are assigned a larger score. It is a simple implementation of
an "active learning" idea.
"""

import argparse
import numpy as np

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.btblabeling.db.datamodel.models import Post, PostScored
from src.utils.utils_db import get_database_connection


parser = argparse.ArgumentParser()
parser.add_argument("--model_type", type=str, default="notML")
NUM_CAT = 21


if __name__ == "__main__":
    args = parser.parse_args()

    db_url = get_database_connection()
    engine = create_engine(db_url)

    Session = sessionmaker(bind=engine)
    session = Session()
    result = session.query(Post).outerjoin(
        PostScored, Post.id == PostScored.ref_id).filter(
        PostScored.ref_id.is_(None))

    if args.model_type != 'ML':
        probs = np.full((1, NUM_CAT), 1/NUM_CAT)
        score = (-probs * np.log2(probs)).sum(axis=1)[0]

    info = []
    for r in result:
        toWrite = PostScored(
            ref_id=r.id,
            title=r.title,
            body=r.body,
            url=r.url,
            language=r.language,
            pubdate=r.pubdate,
            source=r.source,
            score=score)

        info.append(toWrite)
    session.add_all(info)
    session.commit()
