DROP TABLE IF EXISTS NEWSDB;
CREATE TABLE IF NOT EXISTS NEWSDB (
    id                  SERIAL PRIMARY KEY /* News ID */
    ,source             TEXT             /* News source */
    ,url                TEXT UNIQUE      /* News url */
    ,language           TEXT             /* News language */
    ,lastmod            TEXT             /* Last modification date */
    ,pubdate            TEXT             /* News publication date */
    ,title              TEXT             /* News title */
    ,body               TEXT             /* News text */
);

DROP TABLE IF EXISTS NEWSDBSCORE;
CREATE TABLE IF NOT EXISTS NEWSDBSCORE (
    id                    SERIAL PRIMARY KEY
    ,ref_id               INT
    ,source               TEXT             /* News source */
    ,url                  TEXT UNIQUE      /* News url */
    ,language             TEXT
    ,title                TEXT
    ,body                 TEXT
    ,label                TEXT  /* Label of the news article */
    ,score                FLOAT  /* entropy score */
    ,pubdate              TEXT /* news publication time */
    ,is_annotating        BOOLEAN DEFAULT FALSE  /* is being annotated in Label Studio */
);

DROP TABLE IF EXISTS NEWS_ANNOTATED;
CREATE TABLE IF NOT EXISTS NEWS_ANNOTATED (
    id                    SERIAL PRIMARY KEY
    ,ref_id               INT
    ,source               TEXT             /* News source */
    ,url                  TEXT UNIQUE      /* News url */
    ,language             TEXT
    ,title                TEXT
    ,body                 TEXT
    ,label                TEXT  /* Label of the news article */
    ,ann_time             TEXT /* annotation time */
    ,pubdate              TEXT /* news publication time */
    ,exported             BOOLEAN DEFAULT FALSE
);
