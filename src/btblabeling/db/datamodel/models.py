"""Script to define data models or tables for SQLAlchemy"""

from sqlalchemy import Column, Integer, String, Float, Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Post(Base):
    """Describes fields in the Post table."""

    __tablename__ = 'newsdb'
    id = Column(Integer, primary_key=True)
    source = Column(String)
    url = Column(String, unique=True)
    language = Column(String)
    lastmod = Column(String)
    pubdate = Column(String)
    title = Column(String)
    body = Column(String)


class PostScored(Base):
    """Describes fields in the PostScored table."""

    __tablename__ = 'newsdbscore'
    id = Column(Integer, primary_key=True)
    source = Column(String)
    ref_id = Column(Integer)
    url = Column(String, unique=True)
    language = Column(String)
    score = Column(Float)
    pubdate = Column(String)
    title = Column(String)
    body = Column(String)
    is_annotating = Column(Boolean, default=False)


class PostLabeled(Base):
    """Describes fields in the PostScored table."""

    __tablename__ = 'news_annotated'
    id = Column(Integer, primary_key=True)
    source = Column(String)
    ref_id = Column(Integer)
    url = Column(String, unique=True)
    language = Column(String)
    title = Column(String)
    body = Column(String)
    label = Column(String)
    pubdate = Column(String)
    ann_time = Column(String)
    exported = Column(Boolean, default=False)
