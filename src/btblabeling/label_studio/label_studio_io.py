"""This module describes operations related to Label Studio.
It allows
- exporting from a database to Label Studio for labeling.
- exporting from Label Studio to data/raw folder in a form of .txt files
  for further model training (News Classification model uses .txt files
  from the disk).
"""

import argparse
import os

from label_studio_sdk import Client
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError

from src.btblabeling.db.datamodel.models import PostScored, PostLabeled
from src.utils.utils_db import get_database_connection


parser = argparse.ArgumentParser()
parser.add_argument("--task", type=str, default="export")
parser.add_argument("--remove", type=str, default=False)


class LabelStudioIOClass:
    """Class that allows to work with a Label Studio project"""

    def __init__(self, url: str, api: str, project_name: str):
        """Class Init to establish a connection to a postgres db and
        a Label studio project

        Args:
            url (str) - url where label studio is running
            api (str) - API key of the project
            project_name (str) - name of the project in Label Studio
        """

        self.project_name = project_name
        self.client = Client(
            url=url,
            api_key=api
            )
        self.session = self.get_session()

    def get_project(self):
        """Utility function to establish a connect to a
        Label Studio project or create one if it is missing"""

        client = self.client
        projs = client.get_projects()
        for proj in projs:
            if proj.title == self.project_name:
                return proj

        project = client.start_project(
            title=self.project_name,
            label_config='''
            <View>
                <Header value="Choose news class" />
                <Text name="text" value="$text"/>
                    <Choices name="Category" toName="text" choice="single" showInLine="true">
                    <Choice value="Auto"/>
                    <Choice value="Business"/>
                    <Choice value="Children"/>
                    <Choice value="Crime"/>
                    <Choice value="Culture"/>
                    <Choice value="Economics"/>
                    <Choice value="Education"/>
                    <Choice value="Entertainment"/>
                    <Choice value="Environment"/>
                    <Choice value="Games"/>
                    <Choice value="Health"/>
                    <Choice value="Other"/>
                    <Choice value="Politics"/>
                    <Choice value="Religion"/>
                    <Choice value="Science"/>
                    <Choice value="Society"/>
                    <Choice value="Sports"/>
                    <Choice value="Tech"/>
                    <Choice value="Travel"/>
                    <Choice value="Weather"/>
                    </Choices>
            </View>
            '''
        )
        return project

    def get_session(self):
        """Utility function to establish a connection to postgres database"""
        db_url = get_database_connection()
        engine = create_engine(db_url)
        Session = sessionmaker(bind=engine)
        session = Session()
        return session

    def import_tasks(self):
        """Allows to import tasks for annotation into Label Studio interface"""

        session = self.session
        result = session.query(PostScored).filter(
            PostScored.is_annotating.is_(False)).all()
        if result:
            tasks = []
            for res in result:

                _id = str(res.id)
                title = res.title
                body = res.body
                # score = res.score
                url = res.url
                language = 'other' if res.language is None else res.language
                pubdate = res.pubdate
                source = 'None' if res.source is None else res.source
                task = {
                    "ref_id": _id,
                    "data": {
                        "text": title + '[title]\n\n' +
                        url + '[url]\n\n' +
                        language + '[lang]\n\n' +
                        pubdate + '[pubdate]\n\n' +
                        source + '[source]\n\n' +
                        body}
                }
                tasks.append(task)
                session.query(PostScored).filter_by(id=res.id).update(
                    {"is_annotating": True}
                    )

            prj = self.get_project()
            prj.import_tasks(tasks)
            session.commit()

    def parse_export_results(self, ls_output):
        """Label Studio exports tasks in a given format.
        This function allows to parse Label Studio export, so that
        it could be further written into a postgres database."""

        res_output = []

        for res in ls_output:
            news_id = res['id']
            _annotations = res['annotations'][0]['result']
            _results = _annotations[0]
            category = _results['value']['choices'][0]
            update_time = res['updated_at']
            text = res['data']['text'].split('[title]')
            title = text[0]
            aftertitle = text[1].split('[url]')
            url = aftertitle[0].strip()
            afterurl = aftertitle[1].split('[lang]')
            language = afterurl[0].strip()
            afterlanguage = afterurl[1].split('[pubdate]')
            pubdate = afterlanguage[0].strip()
            afterpubdate = afterlanguage[1].split('[source]')
            source = afterpubdate[0].strip()
            body = afterpubdate[1].strip()
            res_output.append((
                news_id,
                category,
                update_time,
                url,
                language,
                pubdate,
                source,
                title,
                body))

        return res_output

    def export_tasks(self):
        """Script to export tasks from a Label Studio project to
        a postgres database."""

        session = self.session
        prj = self.get_project()
        results = prj.export_tasks()
        res_lst = self.parse_export_results(results)

        for news_id, label, ann_time, url, language, pubdate, source, title, body in res_lst:
            to_write = PostLabeled(
                ref_id=news_id,
                title=title,
                body=body,
                label=label,
                ann_time=ann_time,
                url=url,
                language=language,
                pubdate=pubdate,
                source=source,
                exported=False)

            with session.begin():
                try:
                    session.add(to_write)
                    session.commit()
                except IntegrityError:
                    continue

    def remove_labeled_tasks(self):
        """Remove tasks that were annotated from a Label Studio project"""
        prj = self.get_project()
        labeled_task_ids = prj.get_labeled_tasks_ids()
        prj.delete_tasks(labeled_task_ids)


if __name__ == '__main__':

    args = parser.parse_args()
    hostname = os.getenv('LABEL_STUDIO_URL')
    label_studio_port = os.getenv('LABEL_STUDIO_PORT')
    label_studio_api_key = os.getenv('LABEL_STUDIO_API_KEY')
    label_studio_project_name = os.getenv('PROJECT_NAME')
    label_studio_url = f'{hostname}:{label_studio_port}'

    ls = LabelStudioIOClass(
        label_studio_url,
        label_studio_api_key,
        label_studio_project_name)

    if args.task == "import":
        ls.import_tasks()

    if args.task == "export":
        ls.export_tasks()
    if args.remove:
        ls.remove_labeled_tasks()
