"""Script with methods that crawl BBC news web-site"""
from collections import namedtuple
from typing import Union

from bs4 import BeautifulSoup as bs
import requests


article = namedtuple(
    "article",
    ["url", "title", "language", "lastmod", "publication_date"])


class BBC:
    """Class to parse news from www.bbc.co.uk site
    """

    def __init__(self, config):
        self.languages = set(config['languages'])
        self.bodypattern = config['bodypattern']

    def get_soup(self, url: str) -> str:
        """Use beautiful soup library to send a request given an url
        of a news

        Args:
            url (str): url of a news
        Returns:
            A web page data.
        """

        try:
            article = requests.get(url, timeout=3)
        except requests.ConnectionError:
            return None
        soup = bs(article.content, "html.parser")
        return soup

    def get_body(self, soup: str) -> Union[None, str]:
        """Parse news text

        Args:
            soup (str): A result of a request by beautiful soup.

        Returns:
            Union[None, str]: News text if it was parsed.
        """
        soup_of_p = soup.findAll("p", {"class": self.bodypattern})
        if soup_of_p is not None:
            body = [p.text for p in soup_of_p]
            return ' '.join(body)
        return None

    def crawl_one_article(self, url: str) -> str:

        """Crawl a given url and return news text
        """

        soup = self.get_soup(url)
        if soup is None:
            return None
        body = self.get_body(soup)
        return body

    def get_info_from_sitemap(self, url_sitemap: str):
        """Parse a sitemap. Sitemaps usually contain urls of multiple news.

        Args:
            url_sitemap (str): url of a sitemap

        Returns:
            Tuple of parsed articles
        """

        res = requests.get(url_sitemap, timeout=3)
        xml = res.text
        soup = bs(xml, features='xml')
        urlTags = soup.find_all("url")

        info = []
        for urltag in urlTags:
            lang = urltag.find_next('news:language').text
            if lang in self.languages:
                url = urltag.find_next('loc').text
                title = urltag.find_next('news:title').text
                lastmod = urltag.find_next('lastmod').text
                publication_date = urltag.find_next('news:publication_date').text
                lastmod = urltag.find_next('lastmod').text
                a_info = article(url, title, lang, lastmod, publication_date)
                info.append(a_info)
        return info
