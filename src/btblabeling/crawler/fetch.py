"""Entry point to crawling. Receives a command to crawl data."""

import argparse
import time

from sqlalchemy.exc import IntegrityError
from src.btblabeling.crawler.news_crawler.crawler_bbc import BBCCrawler

from writer.PostgresWriterClass import PostgresWriter

CONFIG_PATH = 'src/btblabeling/crawler/config_crawler.yaml'

parser = argparse.ArgumentParser()
parser.add_argument("--source", type=str, default="bbc")

if __name__ == '__main__':

    args = parser.parse_args()
    source = args.source

    writer = PostgresWriter()

    if source == 'bbc':
        bbccrawler = BBCCrawler(CONFIG_PATH)
        posts = bbccrawler.crawl()

    for data, text in posts:

        url, title, language, lastmod, pubdate = data
        try:
            writer.write_post(source, url, language, lastmod, pubdate, title, text)
        except IntegrityError:
            continue
        time.sleep(2.0)
