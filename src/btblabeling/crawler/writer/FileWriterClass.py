import os


class FileWriter:

    def __init__(self, path_to_raw, suffix='.txt'):
        self.path_to_raw = path_to_raw
        self.suffix = suffix

    def write_post(self, data, body):

        dirname = self.path_to_raw
        suffix = self.suffix

        url = data[0]
        title = data[1]
        lang = data[2]
        lastmod = data[3]
        pubdate = data[4]

        filename = '[BBC] ' + pubdate.replace(':', '-') + ' ' + title.replace(':', '-') + suffix

        path = os.path.join(dirname, filename)

        with open(path, 'w', encoding='utf-8') as f:
            f.write(f'{url}\n')
            f.write(f'{lang}\n')
            f.write(f'{lastmod}\n')
            f.write(f'{pubdate}\n\n')
            f.write(f'{title}\n')
            f.write(f'{body}\n')
