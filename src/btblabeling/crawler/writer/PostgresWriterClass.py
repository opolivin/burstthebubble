"""Class that writes data into postrgres database
It is speciic to write data to "newsdb" table. """

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from src.btblabeling.db.datamodel.models import Post
from src.utils.utils_db import get_database_connection


class PostgresWriter:
    """Class to write into postgres table"""

    def __init__(self):

        db_url = get_database_connection()
        self.engine = create_engine(db_url)
        self.session = sessionmaker(bind=self.engine)
        self.base = declarative_base()
        self.base.metadata.create_all(self.engine)
        self.post_writer = Post

    def write_post(self,
                   source,
                   url,
                   language,
                   lastmod,
                   pubdate,
                   title,
                   text):
        """Method to write data into the postgres table"""

        post = self.post_writer(
            source=source,
            url=url,
            language=language,
            lastmod=lastmod,
            pubdate=pubdate,
            title=title,
            body=text)

        with self.session() as sess:
            with sess.begin():
                sess.add(post)
                sess.commit()
