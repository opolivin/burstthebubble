beautifulsoup4 >= 4.12.2
pyyaml >= 5.4
requests >= 2.28
lxml >= 4.9.2
sqlalchemy >= 2.0.10
postgres >= 4.0
python-dotenv >=1.0.0