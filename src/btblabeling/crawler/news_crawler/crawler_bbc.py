"""Describes how to crawl a bbc.com web-site"""

import yaml
from src.btblabeling.crawler.news_parser.BBCnews import BBC
from src.utils.utils_crawler import get_sitemaps


class BBCCrawler:
    """Class that guides BBC crawling"""

    def __init__(self, config_path):
        with open(config_path, 'r', encoding='utf8') as config_file:
            self.config = yaml.safe_load(config_file)

    def check_body(self, body: str) -> bool:
        """Check body. This function could be complicated further.
        It performs a simple check that news text length is larger than
        10 words."""

        if body is None:
            return False

        if len(body.split()) > 10:
            return True
        return False

    def crawl(self):
        """Method to walk through links and collect the news data."""
        config = self.config
        bbc = BBC(config['BBC'])
        sitemaps = get_sitemaps(config['BBC']['sitemap'])
        current_urls = set()
        for sitemap in sitemaps:
            info = bbc.get_info_from_sitemap(sitemap)
            for data in info:

                url = data[0]
                if url in current_urls:
                    continue
                current_urls.add(url)
                text = bbc.crawl_one_article(url)

                if self.check_body(text):
                    yield data, text
