"""Utility functions for fetching news data"""

from typing import List

import requests
from bs4 import BeautifulSoup


def get_sitemaps(url: str) -> List:
    """Function to get a list of sitemaps from a news source url

    Args:
        url (str): url where sitemaps are located

    Returns:
        List: A list of sitemaps
    """

    sitemaps = []
    res = requests.get(url, timeout=3)
    xml = res.text
    soup = BeautifulSoup(xml, features='xml')
    sitemap_tags = soup.find_all("sitemap")
    for sitemap in sitemap_tags:
        sitemaps.append(sitemap.findNext("loc").text)
    return sitemaps
