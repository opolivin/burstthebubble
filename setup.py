from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='A collection of tools for news recommendation',
    author='Oleg Polivin',
    license='',
)
