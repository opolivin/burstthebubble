BurstTheBubble
==============================

A collection of tools for news recommendation

Below I provide the project description. Its structure corresponds to Homeworks in the [MLOps in Production course](https://ods.ai/tracks/ml-in-production-spring-23) by ods.ai


# Homework 3

In the end, it turns out that labeling part is managed by `docker`, and training part is managed by `dvc`. `CLI` is implemented in both parts, let me just comment on which models do use it:

- src/btblabeling:
  - `src/btblabeling/label_studio/label_studio_io.py`

- src/btbtraining
  - `src/btbtraining/data/split_data.py`
  - `src/btbtraining/train/train_classifier.py` <- here not really implemnented, but it works through `config_train.yaml`. Also, it could be later added when I develop the training part a bit more.

I would like to record here the steps I used to set up DVC, and the way I will use it.

## Setting up DVC

```
- poetry add dvc: # I will only use a local storage, not S3 bucket
- dvc init
- Removed data/raw and data/processed from .gitignore
- dvc add data/raw
- dvc add data/processed
- dvc remote add -d localname localpath
- created a dvc.yaml file
- Ready to launch dvc repro!

```


# Homework 2

## 1. Project structure and cookiecutter templates

This project is based on the [cookiecutter data science project template by DrivenData](https://drivendata.github.io/cookiecutter-data-science/). However, I made some changes to add additional structure on top of the one provided by cookiecutter template.

Folders like `data` and `models` are not visible in the repo because they actually do contain data, but they are present in the local repo.

Most important change is in the `src` folder. I divide it into the following sub-directories.`btb` in the folder names stands for the project name `BurstTheBubble`.

```
src
├── btblabeling                       <- Everything related to labeling
│   ├── crawler                       <- Crawling service. For the moment, only BBC is included
│   │   ├── config_crawler.yaml       <- Crawler configuration file
│   │   ├── Dockerfile                <- Crawler works in a Docker
│   │   ├── fetch.py                  <- Starts the crawling job
│   │   ├── news_crawler
│   │   │   └── crawler_bbc.py        <- Actually crawls BBC
│   │   ├── news_parser
│   │   │   └── BBCnews.py            <- Defines BBC crawler
│   │   ├── requirements.txt          <- requirements to be installed with `pip` in Docker
│   │   └── writer
│   │       ├── FileWriterClass.py
│   │       └── PostgresWriterClass.py <- Writes data to a Postgres table
│   ├── db
│   │   ├── datamodel
│   │   │   └── models.py             <- Defines data models for the tables
│   │   └── table_creation_script
│   │       └── create_tables.sql     <- Creates initial tables in Docker
│   ├── __init__.py
│   ├── label_studio
│   │   ├── Dockerfile                <- Label Studio is launched in Docker
│   │   ├── label_studio_io.py        <- Main file that imports data to Label Studio and exports data from it
│   │   └── requirements.txt          <- requirements to be installed in Docker
│   └── task_scoring
│       ├── Dockerfile                <- Defines how to score a given news text. So far the same number for all news
│       ├── requirements.txt          <- requirements to be installed in Docker
│       └── task_score.py             <- Python file to execure news scoring (classification)
├── btbserving                        <- Not written at all. Will be used to serve model, it is Front-end.
│   ├── features
│   │   ├── build_features.py
│   │   └── __init__.py
│   ├── __init__.py
│   ├── models
│   │   ├── __init__.py
│   │   ├── predict_model.py
│   │   └── train_model.py
│   └── visualization
│       ├── __init__.py
│       └── visualize.py
├── btbtraining                      <- Trains a model in PyTorch + Lightning.
│   ├── data
│   │   ├── __init__.py
│   │   ├── make_dataset.py          <- Prepares a dataset, takes labeled data from a postgres table -> writes into data/raw folder
│   │   └── split_data.py            <- Splits data from a data/raw folder into train/validation -> writes the split into data/processed folder
│   └── train
│       ├── config_train.yaml
│       ├── lightning_logs           <- Not visible in the repo
│       │   └── version_0
│       │       ├── checkpoints
│       │       │   └── epoch=3-step=112.ckpt
│       │       ├── hparams.yaml
│       │       └── metrics.csv
│       └── train_classifier.py     <- Runs model training
└── utils
    ├── utils_crawler.py            <- Utility functions for crawler
    └── utils_db.py                 <- Utility functions for database
```

## 2. Codestyle and lightning_logs

This part of the project was done first locally, and then integrated into `GitLab CI`. Notice that I am still not using type checker like `mypy`.

## 3. Code versioning.

See project settings. Try to push to main.

## 4. Creating GitLab CI

I was following the instructions listed [here](https://docs.gitlab.com/runner/install/docker.html) to register own `gitlab runner` that runs in docker.

```
docker volume create gitlab-runner-config
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```
Also, for the moment I had to "cheat" a bit and I am running `pylint` and `flake8` under conditions that allow the pipeline to pass. In particular, I run `pylint` as
```
pylint src/* --fail-under=8
```
So that pipelines fails only if the code is scored under `8`.

As for `flake8` I allowed `max-line-length` to be 100.

It is also important to say here that I take part of the `.gitlab-ci.yml` settings from [Mikhail Kovalchuk's repo](https://gitlab.com/kvlml/contacts_detector/-/blob/master/.gitlab-ci.yml). In particular, my objective was to do the following:
- install Poetry
- Activate virtual environment
- install and cache dependencies as defined in the `pyproject.toml` file
- make available and use those dependencies in any stage of the CI from inside of the virtual environment.
